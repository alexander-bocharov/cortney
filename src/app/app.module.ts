import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {StoreModule} from '@ngrx/store';
import {stateReducer} from './redux/state.reducer';
import {StorageService} from './services/storage.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StoreModule.forRoot({currentState: stateReducer}),

  ],
  providers: [StorageService],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
