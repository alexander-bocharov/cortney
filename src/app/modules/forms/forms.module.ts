import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BookFormComponent} from './book/book-form.component';
import {ReactiveFormsModule} from '@angular/forms';
import {StoreModule} from '@ngrx/store';
import {stateReducer} from '../../redux/state.reducer';

@NgModule({
  declarations: [BookFormComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    StoreModule.forFeature('currentState', stateReducer)
  ], exports: [
    BookFormComponent
  ]
})
export class FormsModule {
}
