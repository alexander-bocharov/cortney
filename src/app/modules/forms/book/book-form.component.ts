import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Book} from '../../../models/book.model';
import {StorageService} from '../../../services/storage.service';

const YearOffset = 1800;

@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html'
})
export class BookFormComponent implements OnInit, OnChanges {

  @Input() book: Book;

  @Output() submitted: EventEmitter<null> = new EventEmitter<null>();

  addBookForm: FormGroup;
  years: number[];
  authors: string[];

  constructor(private fb: FormBuilder, private storage: StorageService) {
    this.addBookForm = this.fb.group({
      id: [this.book ? this.book.id : null],
      name: [this.book ? this.book.name : '', [Validators.required, Validators.maxLength(30)]],
      isbn: [this.book ? this.book.isbn : '', [Validators.required, Validators.pattern('^(?:ISBN(?:-1[03])?:? )?(?=[0-9X]{10}$|(?=(?:[0-9]+[- ]){3})[- 0-9X]{13}$|97[89][0-9]{10}$|(?=(?:[0-9]+[- ]){4})[- 0-9]{17}$)(?:97[89][- ]?)?[0-9]{1,5}[- ]?[0-9]+[- ]?[0-9]+[- ]?[0-9X]$')]],
      year: [this.book ? this.book.year : null],
      authors: new FormArray([this.fb.group({
        firstName: ['', [Validators.required, Validators.maxLength(20)]],
        lastName: ['', [Validators.required, Validators.maxLength(20)]],
      })])
    });
  }

  ngOnChanges() {
    if (this.book) {
      this.addBookForm.reset(this.book);
      this.addBookForm.controls.authors = new FormArray([]);
      for (const author of this.book.authors) {
        this.addAuthor(author.firstName, author.lastName);
      }
    }
  }

  get authorsFormGroup() {
    return this.addBookForm.controls.authors as FormGroup;
  }

  ngOnInit() {
    if (this.book) {
      console.log(this.book);
    }

    const currentDate = new Date();
    const yearLength = currentDate.getFullYear() - YearOffset + 1;
    this.years = Array(yearLength).fill(null).map((item, index) => currentDate.getFullYear() - index);
  }

  addAuthor(firstName?: string, lastName?: string) {
    (this.addBookForm.controls.authors as FormArray).push(this.fb.group({
      firstName: [firstName, [Validators.required, Validators.maxLength(20)]],
      lastName: [lastName, [Validators.required, Validators.maxLength(20)]]
    }));
  }

  removeAuthor(i: number) {
    (this.addBookForm.controls.authors as FormArray).removeAt(i);
  }

  markFormGroupTouched(formGroup: FormGroup) {
    (Object).values(formGroup.controls).forEach(control => {
      control.markAsTouched();

      if (control.hasOwnProperty('controls')) {
        this.markFormGroupTouched(control as FormGroup);
      }
    });
  }

  submitForm() {
    this.markFormGroupTouched(this.addBookForm);
    this.addBookForm.updateValueAndValidity();
    const book = new Book(this.addBookForm.value);

    if (this.addBookForm.valid) {
      if (this.book) {
        this.storage.updateBook(book);
      } else {
        this.storage.addBook(book);
      }
      this.addBookForm.reset();
      this.submitted.emit(null);
    }

  }

}
