import {Component, OnDestroy, OnInit} from '@angular/core';
import {Book} from '../../../models/book.model';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {Store} from '@ngrx/store';
import {AppState} from '../../../redux/app.state';
import {getBookById} from '../../../redux/state.selectors';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
})
export class BookComponent implements OnInit, OnDestroy {
  book: Book;
  private sub: Subscription;

  constructor(private route: ActivatedRoute, public router: Router, private store: Store<AppState>) {
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.store.select(getBookById(params.id)).subscribe((item) => {
        this.book = item as Book;
        console.log(this.book);
      });
    });


  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
