import {Component, Input, OnInit} from '@angular/core';
import {Book} from '../../../models/book.model';
import {Store} from '@ngrx/store';
import {AppState} from '../../../redux/app.state';
import {DeleteBook} from '../../../redux/state.action';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html'
})
export class ItemComponent implements OnInit {

  @Input() book: Book;

  constructor(private store: Store<AppState>) {
  }

  ngOnInit() {
  }

  deleteBook() {
    this.store.dispatch(new DeleteBook(this.book));
  }

}
