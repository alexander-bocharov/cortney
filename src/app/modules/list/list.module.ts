import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ListComponent} from './list.component';
import {RouterModule, Routes} from '@angular/router';
import {ItemComponent} from './item/item.component';
import {FormsModule} from '../forms/forms.module';
import {StoreModule} from '@ngrx/store';
import {StorageService} from '../../services/storage.service';
import {stateReducer} from '../../redux/state.reducer';
import {BookComponent} from './book/book.component';

const routes: Routes = [
  {
    path: '',
    component: ListComponent,
  },
  {
    path: ':id',
    component: BookComponent
  }
];

@NgModule({
  declarations: [ListComponent, ItemComponent, BookComponent],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    StoreModule.forFeature('currentState', stateReducer)
  ],
  exports: [RouterModule],
  providers: [StorageService]
})
export class ListModule {
}
