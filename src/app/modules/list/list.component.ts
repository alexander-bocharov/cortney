import {Component, OnInit} from '@angular/core';
import {Book} from '../../models/book.model';
import {Store} from '@ngrx/store';
import {AppState} from '../../redux/app.state';
import {DtHeaderRow, HeaderCell, HeaderCellType, SortDirection, Sorter} from '../../components/datatable/datatable';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html'
})
export class ListComponent implements OnInit {
  addForm = false;
  books: Book[];
  header: DtHeaderRow;

  constructor(private store: Store<AppState>) {
    this.header = new DtHeaderRow(
      [
        new HeaderCell('Name', HeaderCellType.Sortable, new Sorter(
          'name',
          true,
          SortDirection.UP
        ), 'column col-3'),
        new HeaderCell('Authors', HeaderCellType.Placeholder, null, 'column col-3'),
        new HeaderCell('ISBN', HeaderCellType.Placeholder, null, 'column col-3'),
        new HeaderCell('Year', HeaderCellType.Sortable, new Sorter(
          'year',
          false,
          SortDirection.NONE
        ), 'column col-2')
      ]);
    this.loadUserSorter();
  }

  ngOnInit() {
    this.store.select('currentState').subscribe(state => {
      this.books = state.books;
    });
    this.listBooks();

  }

  listBooks() {
    for (const sorter of this.header.getSorters()) {
      this.books = sorter.sort(this.books) as Book[];
    }
  }


  getSorter(paramName: string) {
    const header: HeaderCell = this.header.getHeader(paramName);
    return header && header.type === HeaderCellType.Sortable ? header.sorter : null;
  }

  setActiveSorter(paramName: string) {
    const selectedSorter: Sorter = this.getSorter(paramName);

    for (const sorter of this.header.getSorters()) {
      sorter.active = false;
    }

    selectedSorter.direction = selectedSorter.direction === SortDirection.UP ? SortDirection.DOWN : SortDirection.UP;
    selectedSorter.active = true;
  }

  loadUserSorter() {
    try {

      const storedSort = JSON.parse(localStorage.getItem(this.header.storageKeySort));
      if (storedSort) {
        this.setActiveSorter(storedSort.name);
        this.header.getHeader(storedSort.name).sorter.direction = storedSort.sorter.direction;
      }
    } catch (e) {
      console.log(e);
    }
  }

  togglerAction(header: HeaderCell) {
    switch (header.type) {
      case HeaderCellType.Sortable:
        this.setActiveSorter(header.name);
        localStorage.setItem(this.header.storageKeySort, JSON.stringify(header));
        break;
      case HeaderCellType.Placeholder:
        break;
    }
    this.listBooks();
  }

}
