import {Component, OnInit} from '@angular/core';
import {StorageService} from './services/storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  title = 'cortney';

  constructor(private storage: StorageService) {
  }

  ngOnInit() {
    this.storage.loadBooks();
  }
}
