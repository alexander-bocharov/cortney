import {v4 as uuid} from 'uuid';
import {Author} from './author.model';

export class Book {

  public name: string;
  public authors: Author[];
  public isbn: string;
  public year?: string | null;
  public id?: string;

  constructor({name, authors, isbn, year = null, id = null}) {
    this.name = name;
    this.authors = authors;
    this.isbn = isbn;
    this.year = year !== 'null' ? year : null;
    this.id = id || uuid();
  }
}

export interface Books {
  books: Book[];
}
