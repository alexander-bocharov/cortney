import {BOOK_ACTION, STATE_ACTION, StateAction} from './state.action';

const initialState = {
  books: []
};

export function stateReducer(state = initialState, action: StateAction) {
  switch (action.type) {

    case BOOK_ACTION.ADD_BOOK:
      return {
        ...state,
        books: [...state.books, action.payload]
      };

    case BOOK_ACTION.DELETE_BOOK:
      return {...state, books: [...state.books.filter(b => b.id !== action.payload.id)]};

    case BOOK_ACTION.UPDATE_BOOK:
      const pos = state.books.findIndex(b => b.id === action.payload.id);
      state.books[pos] = action.payload;
      return {...state, books: [...state.books]};

    case STATE_ACTION.LOAD_STATE:
      return {...state, ...action.payload};


    default:
      return state;
  }
}
