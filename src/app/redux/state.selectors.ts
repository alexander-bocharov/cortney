import {createSelector} from '@ngrx/store';
import {AppState} from './app.state';

const booksItems = (state: AppState) => state.currentState.books;

export const getBookById = (id) => createSelector(booksItems, (allBooks) => {
  if (allBooks) {
    return allBooks.find(item => {
      return item.id === id;
    });
  } else {
    return {};
  }
});
