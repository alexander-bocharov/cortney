import {Book} from '../models/book.model';
import {Action} from '@ngrx/store';

export enum STATE_ACTION {
  LOAD_STATE = 'LOAD_STATE',
  GET_BOOK = 'GET_BOOK',

}

export enum BOOK_ACTION {
  ADD_BOOK = 'BOOK_ADD',
  DELETE_BOOK = 'BOOK_DELETE',
  UPDATE_BOOK = 'BOOK_UPDATE',
}


export class AddBook implements Action {
  readonly type = BOOK_ACTION.ADD_BOOK;

  constructor(public payload: Book) {
  }
}


export class DeleteBook implements Action {
  readonly type = BOOK_ACTION.DELETE_BOOK;

  constructor(public payload: Book) {
  }
}

export class UpdateBook implements Action {
  readonly type = BOOK_ACTION.UPDATE_BOOK;

  constructor(public payload: Book) {
  }
}


export class LoadState implements Action {
  readonly type = STATE_ACTION.LOAD_STATE;

  constructor(public payload) {
  }
}

export class GetBook implements Action {
  readonly type = STATE_ACTION.GET_BOOK;

  constructor(public payload: number) {
  }
}


export type StateAction = AddBook | DeleteBook | UpdateBook | LoadState | GetBook;
