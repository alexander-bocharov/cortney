import {Book} from '../models/book.model';

export interface AppState {
  currentState: {
    books: Book[];
  };
}
