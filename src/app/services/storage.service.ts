import {Injectable} from '@angular/core';
import {Book} from '../models/book.model';
import {Store} from '@ngrx/store';
import {AppState} from '../redux/app.state';
import {AddBook, LoadState, UpdateBook} from '../redux/state.action';
import {Observable} from 'rxjs/index';


enum StorageKeys {
  STATE = 'CRTN_STATE'
}


@Injectable()
export class StorageService {

  constructor(private store: Store<AppState>) {
  }

  getItem<T>(key): Observable<T> {
    return new Observable((observer) => {
      try {
        const data = localStorage.getItem(key);

        observer.next(JSON.parse(data));

      } catch (e) {
        observer.error('Bad Data');
      }

    });
  }

  setItems(key, value): Observable<null> {
    return new Observable((observer) => {
      try {
        const data = JSON.stringify(value, (k, val) => {
          if (val !== 'null') {
            return val;
          }
        });
        localStorage.setItem(key, data);
        observer.next(null);
      } catch (e) {
        observer.error('Cant save');
      }

    });
  }


  loadBooks() {
    this.getItem(StorageKeys.STATE).subscribe((storedState) => {
      this.store.dispatch(new LoadState(storedState));
      this.store.select('currentState', '').subscribe((state) => {
        this.setItems(StorageKeys.STATE, state).subscribe(() => {
        });
      });
    });
  }

  addBook(book: Book) {
    this.store.dispatch(new AddBook(book));
  }

  updateBook(book: Book) {
    this.store.dispatch(new UpdateBook(book));
  }
}

