export enum SortDirection {
  UP = 1,
  DOWN = -1,
  NONE = 0
}

export class Sorter {
  active = false;
  direction: SortDirection = SortDirection.UP;

  constructor(public paramName: string, active?: boolean, direction?: SortDirection) {
    if (active != null) {
      this.active = active;
    }
    if (direction != null) {
      this.direction = direction;
    }
  }

  getDirection(): string {
    return this.direction === SortDirection.UP ? 'up' : (this.direction === SortDirection.NONE ? 'none' : 'down');
  }

  sort(data: any[], subParam?: string) {
    if (this.active === true) {
      data.sort((item1: any, item2: any) => {

        let value1 = item1[this.paramName];
        let value2 = item2[this.paramName];
        const floatVal1 = parseFloat(value1);
        const floatVal2 = parseFloat(value2);
        value1 = !Number.isNaN(floatVal1) ? floatVal1 : value1;
        value2 = !Number.isNaN(floatVal2) ? floatVal2 : value2;

        if (value1 > value2) {
          return this.direction;
        }
        if (value1 < value2) {
          return this.direction * -1;
        }
        return 0;
      });
    }
    return data;
  }

}

export enum HeaderCellType {
  Sortable = 1,
  Placeholder = 3,
}

export class HeaderCell {

  constructor(public name: string,
              public type: HeaderCellType,
              public sorter?: Sorter,
              public addCss?: string,
              public prefix?: string) {

  }

  action() {
    this.sorter.active = true;
    this.sorter.direction = this.sorter.direction = SortDirection.UP ? SortDirection.DOWN : SortDirection.UP;
  }
}


export class DtHeaderRow {
  storageKeySort = '_tg_sort_';
  storageKeyToggler = '_tg_togglers_';

  constructor(public headers: HeaderCell[]) {
  }

  getHeaders(): HeaderCell[] {
    return this.headers;
  }

  getSorters(): Sorter[] {
    const result: Sorter[] = [];
    for (const header of this.headers) {

      if (header.type === HeaderCellType.Sortable) {
        result.push(header.sorter);
      }

    }
    return result;
  }


  getHeader(name: string) {
    for (const header of this.headers) {

      if (header.name === name) {
        return header;
      }

    }
    return null;
  }
}

