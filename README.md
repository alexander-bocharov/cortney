# Cortney

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.8.
## Installation
Make sure you have the [Angular CLI](https://github.com/angular/angular-cli installed globally.

npm install

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `npm run build`. See dist folder.
